from bs4 import BeautifulSoup
import urllib.request
import re

htmlfile = urllib.request.urlopen("https://en.wikipedia.org/wiki/List_of_countries_and_territories_by_land_and_maritime_borders")

htmltext = htmlfile.read()

soup = BeautifulSoup(htmltext, "html.parser")
table = soup.find("table", attrs={"class":"wikitable sortable"})

# The first tr contains the field names.
headings = [th.get_text() for th in table.find("tr").find_all("th")]

datasets = []
for row in table.find_all("tr")[1:]:
    dataset = zip(headings, (td.get_text() for td in row.find_all("td")))
    datasets.append(dataset)

preDB = [[]]
for dataset in datasets:
    for field in dataset:
        if "Country" in field[0]:
            curCountry = field[1]
        elif "Neighboring countries" in field[0]:
            for neighbor in field[1].split("\n"):
                preDB.append([curCountry, neighbor])
 
DB = []                
for border in preDB:
    if border:
        res1 = re.sub(r'\([^)]*\)', '', border[0])
        res1 = re.sub(r'[^a-zA-Z ]', '', res1).strip()
        res2 = re.sub(r'\([^)]*\)', '', border[1])
        res2 = re.sub(r'[^a-zA-Z ]', '', res2).strip()
        border = [res1, res2]
        DB.append(border)
       
        
f = open('irisDB.iris','w')
for border in DB:
    f.write("route('" + border[0] + "', '" + border[1] + "').\n")
f.close()